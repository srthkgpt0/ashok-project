import React, { useState } from 'react';
import { Table, Tag, Space, Form, Input, Select } from 'antd';
import './Home.css';
const Home = () => {
  const [data, setData] = useState([]);
  const [inputDetails, setInputDetails] = useState({
    machineType: '',
    labourType: '',
    rate: '',
  });
  const handleDelete = (type) => {
    const filtered = data.filter((d) => d.machineType !== type);
    console.log(filtered);
    setData([...filtered]);
  };
  const columns = [
    {
      title: 'Machine Type',
      dataIndex: 'machineType',
      key: 'machine',
    },
    {
      title: 'Labour Type',
      dataIndex: 'labourType',
      key: 'age',
    },
    {
      title: 'Rate Per Person/Annum (INR)',
      dataIndex: 'rate',
      key: 'address',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <Space size='middle'>
          <button>Save</button>
          <button onClick={() => handleDelete(record.machineType)}>
            Delete
          </button>
        </Space>
      ),
    },
  ];
  const machineType = [
    { name: 'Radial Drill', value: 'radialDrill' },
    { name: 'Lathe', value: 'lathe' },
    { name: 'Vertical Drill', value: 'verticalDrill' },
    { name: 'Furnace', value: 'furnace' },
  ];
  const labourType = [
    { name: 'Skilled', value: 'skilled' },
    { name: 'Semi-Skilled', value: 'semiSkilled' },
    { name: 'Unskilled', value: 'unSkilled' },
  ];
  const handleSubmit = (e) => {
    e.preventDefault();
    setData([...data, { ...inputDetails }]);
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setInputDetails({
      ...inputDetails,
      [name]: value,
    });
  };
  return (
    <div>
      <div>
        <form onSubmit={handleSubmit}>
          <div className='form'>
            <div className='form-group'>
              <label className='label'>Machine Type</label>

              <select
                name='machineType'
                onChange={(e) => handleChange(e)}
                placeholder='Select'
              >
                <option>Select</option>
                {machineType.map((machine) => (
                  <option value={machine.name}>{machine.name}</option>
                ))}
              </select>
            </div>

            <div className='form-group'>
              <label className='label'>Labour Type</label>
              <select
                onChange={(e) => handleChange(e)}
                name='labourType'
                placeholder='Select'
              >
                <option>Select</option>
                {labourType.map((labour) => (
                  <option value={labour.name}>{labour.name}</option>
                ))}
              </select>
            </div>

            <div className='form-group'>
              <label>Rate Per Person/Annum (INR) :</label>
              <input
                onChange={(e) => handleChange(e)}
                name='rate'
                placeholder='Enter'
              />
            </div>

            <div>
              <button className='addBtn' type='submit'>
                Add
              </button>
            </div>
          </div>
        </form>
      </div>
      <div className='table'>
        <Table dataSource={data} columns={columns} pagination={false} />
      </div>
      <footer className='footer'>
        <div className='button-group'>
          <button className='cancelBtn'>Cancel</button>
          <button className='addBtn'>Save</button>
        </div>
      </footer>
    </div>
  );
};

export default Home;
